package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CIntegerArrayList implements ISumable{
    ArrayList<Integer> mIntegerArrayList = new ArrayList<>();


    public ArrayList<Integer> getmIntegerArrayList() {
        return mIntegerArrayList;
    }

    public void setmIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
        this.mIntegerArrayList = mIntegerArrayList;
    }

    public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
        this.mIntegerArrayList = mIntegerArrayList;
    }

    public CIntegerArrayList() {
    }
    
    /**
     * @return
     */
    public String getSum() {
        int sum = 0;
        for (int i=0; i<mIntegerArrayList.size(); i++){
            sum += mIntegerArrayList.get(i);
    }
        return "Đây là Sum của class CIntegerArrayList: "+sum;
    }
}
